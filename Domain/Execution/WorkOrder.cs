﻿using System.Collections.Generic;
using Domain.Common;

namespace Domain.Execution
{
    public class WorkOrder : Entity
    {
        internal WorkOrder()
        {
        }

        public ICollection<WorkOrderEntry> WorkOrderEntries { get; } = new List<WorkOrderEntry>();


        public void Cancel()
        {
            foreach(var workOrderEntry in WorkOrderEntries)
            {
                workOrderEntry.MarkAsDeleted();
            }
        }
    }
}