using System.Collections.Generic;
using Domain.Common;

namespace Domain.Execution
{
    public class WorkOrderEntry : Entity
    {
        internal WorkOrderEntry()
        {
            
        }
        
        public ICollection<CompartmentOrderEntry> CompartmentOrderEntries { get; } = new List<CompartmentOrderEntry>();
        
        public void MarkAsDeleted()
        {
            CompartmentOrderEntries.Clear();
        }
    }
}